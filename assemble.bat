@echo off

set PROJECT_CODENAME=gogobox

set /p TREEISH=Enter version (leave blank for master):
if "%TREEISH%" == "" set TREEISH=master

del .build-archive-temp.zip
rd /S /Q .build-temp
mkdir .build-temp

%GIT_HOME%\bin\git archive %TREEISH% --format zip --output .build-archive-temp.zip
build-tools\unzip .build-archive-temp.zip -d .build-temp
del .build-archive-temp.zip

cd .build-temp

cmd /c gradlew.bat clean assembleRelease

if %ERRORLEVEL% EQU 0 goto :done

:error
echo ERROR
goto end

:done
copy /Y app\build\outputs\apk\app-release.apk ..\%PROJECT_CODENAME%.apk
goto end

:end
cd..
rd /S /Q .build-temp

:die
