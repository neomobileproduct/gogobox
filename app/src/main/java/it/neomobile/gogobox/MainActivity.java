package it.neomobile.gogobox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class MainActivity extends Activity {
    private String mGAProductId;
    private Tracker mTracker;
    public static final String APP_ALREADY_INSTALLED = "it.gogobox_already_installed";
    private WebView mWebView;
    private ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setupAnalytics();

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setMax(100);

        setValue(0);


        mWebView = (WebView) findViewById(R.id.web_view);
        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new MyWebViewClient());
        mWebView.setWebViewClient(new MyWebClient());
        mWebView.loadUrl(getString(R.string.url));
    }

    private void setupAnalytics() {
        //send analytics
        mGAProductId = getString(R.string.ga_product_id);
        mTracker = GoogleAnalytics.getInstance(this).newTracker(R.xml.ga_tracker);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        boolean isAppInstalled = sharedPreferences.getBoolean(APP_ALREADY_INSTALLED, false);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (!isAppInstalled) {
            editor.putBoolean(APP_ALREADY_INSTALLED, true);
            editor.commit();
            mTracker.send(new HitBuilders.EventBuilder().setCategory("App").setAction("Installation").setCustomDimension(1, mGAProductId).build());
        } else {
            mTracker.send(new HitBuilders.EventBuilder().setCategory("App").setAction("Launch").setCustomDimension(1, mGAProductId).build());
        }
    }

    private class MyWebViewClient extends WebChromeClient {


        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }
    }

    private class MyWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgress.setVisibility(View.VISIBLE);
            setValue(0);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url != null && !url.startsWith("htt")) {
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgress.setVisibility(View.INVISIBLE);
            setValue(0);

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //pagina di errore custom
            String errorHtml =
                    "<html align=\"center\"> " +
                            "<head></head> " +
                            "<body>  " +
                            "<img src=\"file:///android_res/drawable/connection_error.png\"/> " +
                            "<p>" + getString(R.string.error_message) + "</p> " +
                            "<a href=" + failingUrl + ">" +
                            "<u>" + getString(R.string.error_retry) + "</u> </a>" +
                            " </body> " +
                            "</html>";
            mWebView.loadDataWithBaseURL(null, errorHtml, "text/html", "UTF-8", null);
            mProgress.setVisibility(View.INVISIBLE);
            setValue(0);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView != null) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    public void setValue(int progress) {
        mProgress.setProgress(progress);
    }
}
